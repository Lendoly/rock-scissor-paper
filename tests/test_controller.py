import pytest

from controllers.controller import RockScissorPaperController
from exceptions import MachineOptionsNoInitialized, WrongOption


def test_init_rsp_controller():
    controller = RockScissorPaperController()
    assert not controller.wins
    assert not controller.loses
    assert not controller.draws


def test_handle_lose_rsp_controller():
    controller = RockScissorPaperController()
    controller._handle_lose()

    assert controller.loses == 1
    assert not controller.wins
    assert not controller.draws


def test_handle_win_rsp_controller():
    controller = RockScissorPaperController()
    controller._handle_win()

    assert controller.wins == 1
    assert not controller.loses
    assert not controller.draws


def test_handle_draw_rsp_controller():
    controller = RockScissorPaperController()
    controller._handle_draw()

    assert controller.draws == 1
    assert not controller.loses
    assert not controller.wins


def test_choose_option_rsp_controller():
    controller = RockScissorPaperController()

    assert not controller.machine_option

    controller.choose_option()

    assert controller.machine_option


def test_get_user_options_1_rsp_controller():
    controller = RockScissorPaperController()
    option = 1
    controller.user_option = option
    assert controller.get_user_option == controller.OPTIONS.get(option)


def test_get_user_options_2_rsp_controller():
    controller = RockScissorPaperController()
    option = 2
    controller.user_option = option
    assert controller.get_user_option == controller.OPTIONS.get(option)


def test_get_user_options_3_rsp_controller():
    controller = RockScissorPaperController()
    option = 3
    controller.user_option = option
    assert controller.get_user_option == controller.OPTIONS.get(option)


def test_get_machine_options_1_rsp_controller():
    controller = RockScissorPaperController()
    option = 1
    controller.machine_option = option
    assert controller.get_machine_option == controller.OPTIONS.get(option)


def test_get_user_machine_2_rsp_controller():
    controller = RockScissorPaperController()
    option = 2
    controller.machine_option = option
    assert controller.get_machine_option == controller.OPTIONS.get(option)


def test_get_machine_options_3_rsp_controller():
    controller = RockScissorPaperController()
    option = 3
    controller.machine_option = option
    assert controller.get_machine_option == controller.OPTIONS.get(option)


def test_handle_input_throw_exception_machine_not_initialized_rsp_controller():
    controller = RockScissorPaperController()
    with pytest.raises(MachineOptionsNoInitialized):
        controller.handle_input(1)


def test_handle_input_throw_exception_wrong_options_rsp_controller():
    controller = RockScissorPaperController()
    with pytest.raises(WrongOption):
        controller.handle_input(0)

    with pytest.raises(WrongOption):
        controller.handle_input(999)

# ---------------------
def test_handle_input_user_wins_rock_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 1
    controller.machine_option = 2
    winner, message = controller.handle_input(user_option)
    assert winner == controller.user_option
    assert message == controller.WIN_DICT.get(winner)[1]


def test_handle_input_user_draws_rock_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 1
    controller.machine_option = 1
    winner, message = controller.handle_input(user_option)
    assert not winner
    assert message == controller.DRAW_MESSAGE


def test_handle_input_user_loses_rock_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 1
    controller.machine_option = 3
    winner, message = controller.handle_input(user_option)
    assert winner == controller.machine_option
    assert message == controller.WIN_DICT.get(winner)[1]


# ---------------------
def test_handle_input_user_wins_scissors_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 2
    controller.machine_option = 3
    winner, message = controller.handle_input(user_option)
    assert winner == controller.user_option
    assert message == controller.WIN_DICT.get(winner)[1]


def test_handle_input_user_draws_scissors_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 2
    controller.machine_option = 2
    winner, message = controller.handle_input(user_option)
    assert not winner
    assert message == controller.DRAW_MESSAGE


def test_handle_input_user_loses_scissors_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 2
    controller.machine_option = 1
    winner, message = controller.handle_input(user_option)
    assert winner == controller.machine_option
    assert message == controller.WIN_DICT.get(winner)[1]


# ---------------------
def test_handle_input_user_wins_paper_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 3
    controller.machine_option = 1
    winner, message = controller.handle_input(user_option)
    assert winner == controller.user_option
    assert message == controller.WIN_DICT.get(winner)[1]


def test_handle_input_user_draws_paper_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 3
    controller.machine_option = 3
    winner, message = controller.handle_input(user_option)
    assert not winner
    assert message == controller.DRAW_MESSAGE


def test_handle_input_user_loses_paper_rsp_controller():
    controller = RockScissorPaperController()
    user_option = 3
    controller.machine_option = 2
    winner, message = controller.handle_input(user_option)
    assert winner == controller.machine_option
    assert message == controller.WIN_DICT.get(winner)[1]