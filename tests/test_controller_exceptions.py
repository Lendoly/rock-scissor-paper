from exceptions import MachineOptionsNoInitialized, WrongOption


def test_text_error_machine_options_no_initialized():
    error = MachineOptionsNoInitialized()
    assert str(error) == 'Wrong usage, first machine_option needs to be initialized'


def test_text_error_wrong_option():
    error = WrongOption()
    assert str(error) == "option is not between 1 and 3"
