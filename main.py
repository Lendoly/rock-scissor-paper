import argparse

from interfaces.terminal_user_interface import RockScissorPaper


def get_args():
    parser = argparse.ArgumentParser(
        description="Main to launch flask server or console game",
        epilog="example: python main.py terminal",
    )

    parser.add_argument(
        "interface",
        action="store",
        help="Which interface to execute options are: terminal and flask",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    if args.interface.lower() == "terminal":
        game = RockScissorPaper()
        game.start()
    else:
        print("interface invalid the only possible values are: terminal and flask")
