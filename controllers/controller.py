import random

from exceptions import MachineOptionsNoInitialized, WrongOption


class RockScissorPaperController:
    """
    Controller for the Game Rock Scissor Paper
    """
    last_result = None

    DRAW_MESSAGE = "draw"

    OPTIONS = {
         1: "rock",
         2: "scissors",
         3: "paper"
    }

    FINALS = (
        "win",
        "lose",
        "draws"
    )

    WIN_DICT = {
        1: (2, "blunts"),
        2: (3, "cuts"),
        3: (1, "wraps")
    }

    machine_option = None
    user_option = None

    def __init__(self):
        """
        Start a new game and initialize the counters to 0
        :return: None
        """
        self.wins = 0
        self.loses = 0
        self.draws = 0

    def _handle_lose(self):
        """
        Method that handle the business logic if the player loses
        :return: None
        """
        self.loses += 1
        self.last_result = self.FINALS[1]

    def _handle_win(self):
        """
        Method that handle the business logic if the player wins
        :return: None
        """
        self.wins += 1
        self.last_result = self.FINALS[0]

    def _handle_draw(self):
        """
        Method that handle the business logic if the player draws
        :return:
        """
        self.draws += 1
        self.last_result = self.FINALS[2]

    def handle_input(self, user_option: int):
        """
        Given an option it checks if the user wins or not
        :param user_option: option choose by the user
        :return: message
        """
        if user_option not in self.WIN_DICT:
            raise WrongOption

        if not self.machine_option:
            raise MachineOptionsNoInitialized

        self.user_option = user_option
        win_to, message = self.WIN_DICT.get(user_option)
        if win_to == self.machine_option:
            self._handle_win()
            return user_option, message

        win_to, message = self.WIN_DICT.get(self.machine_option)
        if win_to == user_option:
            self._handle_lose()
            return self.machine_option, message

        self._handle_draw()
        return None, self.DRAW_MESSAGE

    def choose_option(self):
        """
        Method to random choose the machine option from the three possibilities
        :return:
        """
        self.machine_option = random.choice(list(self.OPTIONS.keys()))

    @property
    def get_user_option(self):
        return self.OPTIONS.get(self.user_option)

    @property
    def get_machine_option(self):
        return self.OPTIONS.get(self.machine_option)
