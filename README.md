# rock scissor paper

Python implementation of the classic game in a terminal

## Usage
To run it in the terminal just use the follow the next steps:
- Create a virtualenv (optional but recommended)
- Run the main file as follows `$ python main.py terminal`

To run the tests:
- Create (or use existent one) a virtualenv (optional but recommended)
- Install the requirements if there are not already installed `$ pip install -r requirements.txt`
- Run tox with `$ tox`

Once the tests run to see the coverage report run:
- Create (or use existent one) a virtualenv (optional but recommended)
- Install the requirements if there are not already installed `$ pip install -r requirements.txt`
- Run tox coverage report with with `$ tox -e coverage_report`
- new folder called htmlcov will be generated open the index.html to see an interactive report


## Interfaces

This implementations only provide a terminal interface but since is mounted following a MVC architecture
can be easily expanded with, for example, and API interface

At the start it will show a little piece of text describing what is the game about and will print
 the actual status of the game (num of wins, loses and draws) and the options that tge user can choose
to the user:
  
0. Exit
1. Rock
2. Scissors
3. Paper

If the user choose the option 0 the game ends
If the user choose the option 1 the game understand that the user is choosing rock
If the user choose the option 2 the game understand that the user is choosing scissors
If the user choose the option 3 the game understand that the user is choosing paper

After the user picking his option the machine will pick one posibility randomly and will check who wins
updating the status of the current game into the game status


## Future imprrovements

- Store game in database
- API interface
- GUI interface
