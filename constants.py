DATABASE_HOST = "sqlite:///games.db"

GAME_OVER_MESSAGE = """
      ::::::::     :::      :::   :::  ::::::::::
    :+:    :+:  :+: :+:   :+:+: :+:+: :+:
   +:+        +:+   +:+ +:+ +:+:+ +:++:+
  :#:       +#++:++#++:+#+  +:+  +#++#++:++#
 +#+   +#+#+#+     +#++#+       +#++#+
#+#    #+##+#     #+##+#       #+##+#
######## ###     ######       #############
      :::::::: :::     ::::::::::::::::::::::
    :+:    :+::+:     :+::+:       :+:    :+:
   +:+    +:++:+     +:++:+       +:+    +:+
  +#+    +:++#+     +:++#++:++#  +#++:++#:
 +#+    +#+ +#+   +#+ +#+       +#+    +#+
#+#    #+#  #+#+#+#  #+#       #+#    #+#
########     ###    #############    ###
"""


TEXT_GAME_OPTIONS = """
Which Option do you want:
    0 - Exit
    1 - Rock
    2 - Scissor
    3 - Paper
==> Option to run (eg: 1)
==> """

TEXT_NEW_GAME = f"""
Starting Rock-Scissors-Paper game, In case you never had childhood and played it to choose how was the next goalkeeper
here is brief summary:

    - Two people (machine and people in this case) fight against each other.
    - Both choose one of the options (Rock, Scissors or Paper) at the same time  and use his hand for identify it
        • Fist for rock
        • Open hand for paper
        • Showing the index and middle finger equals scissors.
    - The winner is determined by the following schema:
        • Paper beats (wraps) rock
        • Rock beats (blunts) scissors
        • Scissors beats (cuts) paper.

Good luck!
"""
