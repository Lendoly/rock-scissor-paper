from os import system

from constants import GAME_OVER_MESSAGE, TEXT_NEW_GAME, TEXT_GAME_OPTIONS
from controllers.controller import RockScissorPaperController
from exceptions import WrongOption, MachineOptionsNoInitialized


class RockScissorPaper:
    player_name = None
    winner = None
    message = None
    option = None

    def __init__(self):
        self.controller = RockScissorPaperController()

    def _clear(self):
        """
        Clean the terminal
        :IMPORTANT: This only clean the terminal on Linux and MacOX probably will make it fail on Windows
        """
        _ = system("clear")

    def _print_status(self):
        """
        print the current status of the game
        """
        if self.controller.last_result:
            print(f"You: {self.controller.last_result}")
            if not self.winner:
                result_str = "the machine also"
            elif self.winner == self.option:
                result_str = f"{self.controller.get_user_option}  {self.message} {self.controller.get_machine_option}"
            else:
                result_str = f"{self.controller.get_machine_option} {self.message} {self.controller.get_user_option}"
            print(f"Because you choosed {self.controller.get_user_option} and {result_str}")
        print(f"Wins: {self.controller.wins} - Loses: {self.controller.loses} - Draws: {self.controller.draws}")

    def _print_game_over(self):
        print(GAME_OVER_MESSAGE)

    def _print_new_game_message(self):
        print(TEXT_NEW_GAME)

    def start(self):
        """
        Start the game by terminal showing the first possible options
        """
        self._print_new_game_message()

        end = False
        input_error = False

        # loop that will keep the game going until is finished
        while not end:
            if input_error:
                print("wrong option, choose one between 0 and 3")
                input_error = False
            self._print_status()

            # print(self.controller.get_word_with_underscores())
            try:

                self.option = int(input(TEXT_GAME_OPTIONS))
                if self.option == 0:
                    end = True
                elif self.option < 0 or self.option > 3:
                    input_error = True
                else:
                    self.controller.choose_option()

                    self.winner, self.message = self.controller.handle_input(int(self.option))
            except (TypeError, ValueError, WrongOption, MachineOptionsNoInitialized):
                input_error = True

            self._clear()
        self._print_game_over()
