class MachineOptionsNoInitialized(BaseException):
    def __str__(self):
        return 'Wrong usage, first machine_option needs to be initialized'


class WrongOption(BaseException):
    def __str__(self):
        return "option is not between 1 and 3"
